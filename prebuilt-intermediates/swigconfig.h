/* Originally generated with:
 *
 * ./configure --without-pcre --without-lua --without-go --without-perl5
 *             --without-java --without-ruby --without-boost --without-octave
 *             --without-scilab --without-guile --without-android
 *             --without-javascript --without-tcl --without-php
 *             --without-mzscheme --without-ocaml --without-r --without-d
 *             --without-csharp --enable-cpp11-testing
 *
 * Then hand-edited to include only macros used by source files.
 */

#define HAVE_POPEN 1

#define PACKAGE_BUGREPORT "http://www.swig.org"
#define PACKAGE_VERSION "4.0.1"

#define SWIG_CXX "clang"

#define SWIG_LIB ""

#define SWIG_PLATFORM "x86_64-pc-linux-gnu"
